# PoC Implementation: "Accurate and Efficient Continuous Time and Discrete Events Simulation in SystemC" 

## Description:
This repository contains an implementation of the algorithms described in the paper *[Accurate and Efficient Continuous Time and Discrete Events Simulation in SystemC](https://ieeexplore.ieee.org/document/9116300/)*, published in the DATE'20 conference.
It contains, in particular, the source code of:
* The continuous time and discrete events synchronization algorithm.
* The following three case studies: 
    1. Vehicle's Automatic Transmission Control 
    2. Bouncing Ball
    3. Switched RC Circuit

The synchronization algorithm is located in the `layer` folder.
It is implemented  by the `synchronization_layer` class.
Apart from this class, a set of other classes build up the infrastructure for modeling and simulation of CT/DE systems that we use in the case studies: 
* `numerical_solver`: class implementing the integration and root location method.
* `ode_system`: abstract class that declares common functions to the CT modules, such as Get-Derivatives, Execute-Updates, Is-Event and Generate-Outputs. 
* `ct_state`: class that eases the manipulation of the CT state.
* `inputs_manager`: class that manages input events to the CT modules. 
* `ct_module`: class that links the SystemC `sc_module` class to the `ode_system` class and that serves as a base class for CT modules. 

Please note that this implementation is a proof of concept and it is neither intended to serve as a reference implementation nor it is suitable for direct use in industrial applications. Its only aim is to support the claim that the algorithms described in our paper are feasible to implement.

The three case studies are located in the `examples` folder. 
For each case study, we provide our model in the `ct_de` folder, the Timed Dataflow Model in the `tdf` folder, and the MATLAB/Simulink reference model in the `matlab` folder. For compilation and execution of these case studies, see the instructions below.


## Dependencies:
1. [SystemC 2.3.1a.](https://www.accellera.org/images/downloads/standards/systemc/systemc-2.3.1a.tar.gz)
2. Boost libraries, to install on debian-based systems execute `sudo apt-get install libboost-all-dev`
3. [Eigen libraries] (http://eigen.tuxfamily.org/index.php?title=Main_Page)
3. (Optional, for comparison purposes) [SystemC AMS](https://www.coseda-tech.com/systemc-ams-proof-of-concept) compiled against SystemC 2.3.1a. 
4. (Optional, for comparison purposes) MATLAB/Simulink.
4. (Optional) GNU Octave.


## Execution:

In the `ct_de` and `tdf` folders of each case study:

1. In the `Makefile`, set the SYSTEMC_PREFIX and SYSTEMC_AMS_PREFIX environment variables with the SystemC and SystemC AMS installation paths. Set the EIGEN_INC_DIR with the include directory of the Eigen installation. 


2. Execute `make` to compile:
```console
foo@bar:~$ make 
```

3. Simulate:
```console
foo@bar:~$ ./main
```

4. (Optional) Run the Octave script to display the results:
```console
foo@bar:~$ octave results.m 
```

For the MATLAB/Simulink version of the Vehicle's Automatic Transmission Control case study, the script `examples/vehicle_cruise_control/matlab/parameter_values.m` has to be run before executing the Simulink model. Its purpose is to load the model's parameter values. 

## Testing your own examples:

To add and simulate your own model you can follow the simple example of an RC Circuit in the `examples/rc_circuit/ct_de` folder. You will need to:

1. Include the `ct_module.h` header: 
```cpp
#include "ct_module.h"
```

2. Make your CT module inherit from `sct_core::ct_module`:
```cpp
class YourCtModule : public sct_core::ct_module {
    ...
};
```

3. In this module, define all boolean (`sc_core::sc_out<bool>`, `sc_core::sc_in<bool>`) and double ports (`sc_core::sc_out<double>`, `sc_core::sc_in<double>`) in the usual SystemC way. 

4. Define the following module's member functions:  
    * `get_derivatives`, which takes in the state and time and returns the derivative values.
    * `is_event`, returns true if the given state, inputs, and time meet at least one of the state event conditions and false otherwise.
    * `execute_updates`, produces discontinuous updates to the state triggered by input events; it returns true if there is at least one update and false otherwise.
    * `generate_outputs`, maps the state, inputs and global simulation time to the set of outputs.

5. Configure the type of synchronization needed, by calling the functions `set_max_timestep`, `use_adaptive_sync`, and `avoid_rollbacks`, from the `set_sync_parameters` module member function. The maximum timestep refers to the maximum allowed interval at which the CT model runs before generating an output and it is optional. Adaptive synchronization allows to modify this maximum timestep dynamically as the simulation runs to achieve maximum simulation speed. Avoiding rollbacks mean that the CT part will use the time of the next discrete event to set the length of the interval it runs so as to avoid rolling back when the event occurs; it should be used only if there are relatively few discrete events  that do not affect the CT model, otherwise simulation speed will degrade.

6. You can optionally define the `get_next_predictable_event_time` if your model produces predictable events in time, such as sampling. An example is given in the `examples/rc_circuit/ct_de` folder.