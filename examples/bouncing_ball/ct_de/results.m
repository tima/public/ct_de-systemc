trace_1 = load("ball_ct.dat");
load("../matlab/simulink_bouncing.mat");
t_matlab = ans(1,:);
x_matlab = ans(2,:);
v_matlab = ans(3,:);
trace_tdf = load("../tdf/bouncing_tdf.dat");

figure(1);
hold on;
grid on;
plot(trace_1(:,1), trace_1(:,2) , '-r', 'LineWidth', 2);
plot(trace_1(:,1), trace_1(:,3) , '-k', 'LineWidth', 1);
plot(t_matlab, x_matlab , '--b', 'LineWidth', 2);
plot(trace_tdf(1:40:end,1), trace_tdf(1:40:end,2) , 'og', 'LineWidth', 2);
set(gca,'XTick', [0:1.5:15]);
xlim([ 0.000 15]);

legend("Height", "Velocity", "MATLAB", "TDF 0.01 s");

drawnow();

input("Press enter");