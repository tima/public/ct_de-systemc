#ifndef BALL_CT_H
#define BALL_CT_H

#include <systemc>
#include "ct_module.h"
#include <cmath>

#include "ball_common.h"

// Order of the system of diff. equations
#define ORDER 2 
// Length of integration interval
#define DELTA_T 0.1 // seconds


class BallCt : public sct_core::ct_module { 
    public:
        //////////////////////////////////////////////////////////////
        // To save traces with the usuar SystemC AMS mechanism.
        // We need to provide a mechanism that does not depend on 
        // signals to record the CT state, because the CT module 
        // communicates with the external world through events 
        // that do not necessarily contain all the information we might
        // want to record about the internals of the CT module
        //////////////////////////////////////////////////////////////

        // Ball's height
        sc_core::sc_out<double> h_out;
         // Ball's velocity 
        sc_core::sc_out<double> v_out;
        
        //////////////////////////////////////////////////////////////
        // Output events 
        //////////////////////////////////////////////////////////////

        // Each transition signals a collision event
        sc_core::sc_out<bool> collision_out;

        // The first transition from false to true signals
        // the low energy event
        sc_core::sc_out<bool> low_energy_out;


        //////////////////////////////////////////////////////////////
        // Input events 
        //////////////////////////////////////////////////////////////

        // Each transition signals a collision and, consequently, 
        // that the ball should bounce
        sc_core::sc_in<bool> bounce_in;
        // A transition from false to true means that the ball goes 
        // from Bouncing to Stopped
        sc_core::sc_in<bool> stop_in;

        
        //////////////////////////////////////////////////////////////
        // Public methods 
        //////////////////////////////////////////////////////////////

        BallCt(sc_core::sc_module_name name, 
            bool use_adaptive = true, bool avoid_rollback = true, 
            double sync_step = DELTA_T) :
            use_adaptive(use_adaptive), 
            avoid_rollback(avoid_rollback),
            sync_step(sync_step)
        {
        
        }


        void set_sync_parameters(){  
            sync_step = sync_step <= 0 ? DELTA_T : sync_step;
            
            set_max_timestep(sync_step);
            use_adaptive_sync(use_adaptive);
            avoid_rollbacks(avoid_rollback);
        }

        void set_initial_conditions(){
            x.resize(2);
            x[0] = 10;
            x[1] = 0;
        }

        void get_derivatives(bool use_input_checkpoints,
            const sct_core::ct_state &x , sct_core::ct_state &dxdt , double t ){
            
            // Set the mode of the inputs manager to either current_values or checkpoint
            ode_system::inputs.use_checkpoints(use_input_checkpoints);
            // Get the value from the stop_in port
            bool stopped = ode_system::inputs[stop_in];
            
            // Bouncing.
            if (!stopped) { 
                dxdt[0] = x[1]; 
                dxdt[1] = -g;  
            }
            // Stopped, all derivatives to zero.
            else {
                dxdt[0] = 0; 
                dxdt[1] = 0;   
            }

        }

        // Threshold crossing condition.
        std::unordered_map<int, bool> is_event(const sct_core::ct_state &x, double t = 0){
            std::unordered_map<int, bool> events;

            bool stopped = ode_system::inputs[stop_in];
            
            // Bouncing.
            if (!stopped) { 
                // Condition for the bounce event
                events[collision_event] =  x[0] <= min_x_threshold && x[1] < 0;
                
                events[low_energy_event] = x[0] <= min_x_threshold && fabs(x[1]) < min_v_threshold;
            }

            return events;
        }

        bool execute_updates() {
            // Condition for the state update
            // Notice that last_bounce_in_value should not constitute
            // a DE state inside the CT module. The problem here is with 
            // the channel : we are not allowed to consume the event 
            // of bouncing, so we need to base our login on the transition 
            // of the boolean variable. This has to be solved with the 
            // definition and implementation of appropriate channels. 
            if(bounce_in.read() != last_bounce_in_value){
                last_bounce_in_value = bounce_in.read();
                // Discontinuity in state
                x[1] = -elasticity_factor*x[1];
                // Return true to indicate the discontinuous change 
                // to the synch. algorithm
                return true;
            }
            // No update
            return false;
        }


        void generate_outputs(bool state_event_located,
            std::unordered_map<int, bool> events
        ) {
            h_out.write(x[0]);
            v_out.write(x[1]);

            // If it is a low energy event
            if (events[low_energy_event]) {
                low_energy_out.write(true);
            }

            // If it is a collision event and it has been located
            if (state_event_located && events[collision_event]) {
                collision_out.write(!collision_out.read());
            }
        }

    private:
        // System parameters
        double g = 9.81;
        double min_x_threshold = 0.001;
        double min_v_threshold = 0.001;
        double elasticity_factor = 0.8;

        // Foolish var. Needed because we still do not count with 
        // appropriate channels to transmit events and their values.
        bool last_bounce_in_value;

        typedef enum {collision_event, low_energy_event} event_ids;

        // Sync. parameters
        bool use_adaptive, avoid_rollback; 
        double sync_step;


};

#endif