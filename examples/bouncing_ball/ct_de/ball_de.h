#ifndef BALL_DE_H
#define BALL_DE_H

#include <systemc>
#include "ball_common.h"

class BallDe : public sc_core::sc_module { 
    public:
        // A transition from false to true means that the ball should 
        // stop bouncing
        sc_core::sc_out<bool> stop_out;
        // A transition from false to true means that the integration
        // procedure failed to find the time of the bouncing
        sc_core::sc_in<bool> low_energy_in; 

        int bounce_count;

        BallDe(sc_core::sc_module_name name)
        { 
            SC_HAS_PROCESS(BallDe);

            SC_METHOD(stop_bouncing);
            sensitive << low_energy_in;
            dont_initialize();
            bounce_count = 0;

            state = running;
        }

        // Output a signal to stop the ball 
        // bouncing when the numerical solver 
        // is not capable of finding the state event
        void stop_bouncing(){
            state = stopped;
            stop_out.write(true);
            std::cout << "Writing stop out " << std::endl;
        }

    private : 
       BallState state;


};

#endif