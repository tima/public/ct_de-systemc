#include<systemc>
#include<iostream> 
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "ball_ct.h"
#include "ball_de.h"


#include "sync_conf_utility.h"

// Pass as first parameter y to use the adaptive sync. strategy,
// y as second parameter to avoid rollbacks (use time of the next
// event), and a real number as the third parameter to set the timestep
int sc_main(int argc, char* argv[]){
    clock_t t = clock();

      sc_core::sc_time simulated_time(15, sc_core::SC_SEC);
    bool use_adaptive = true;
    bool avoid_rollback = false;
    double timestep = 0;

    get_sync_conf(argc, argv, use_adaptive, avoid_rollback, timestep);


    sc_core::sc_signal<double> h_out_sig, v_out_sig;
    sc_core::sc_signal<bool> collision_sig, stop_sig, low_energy_sig;
   

    BallDe ball_de("ball_de");
    BallCt ball_ct("ball_ct", use_adaptive, avoid_rollback, timestep);

    ball_de.stop_out.bind(stop_sig);
    ball_de.low_energy_in.bind(low_energy_sig);


    ball_ct.h_out.bind(h_out_sig);
    ball_ct.v_out.bind(v_out_sig);
    // Notice that bounce_in receives a boolean signal that is 
    // driven by the same object.
    ball_ct.collision_out.bind(collision_sig);
    ball_ct.bounce_in.bind(collision_sig);
    ball_ct.stop_in.bind(stop_sig);
    ball_ct.low_energy_out.bind(low_energy_sig);

    // Start simulation for a given time
    sc_core::sc_start(simulated_time);

    // Finish simulation
    sc_core::sc_stop();


    t = clock() - t;

    print_sync_stats(use_adaptive, avoid_rollback, timestep, simulated_time.to_seconds(), ((double)t)/CLOCKS_PER_SEC);

    return 0;
}


// #include <algorithm>
// #include <vector>
// #include <set>
// #include <boost/operators.hpp>
// #include <boost/numeric/odeint.hpp>


// //[my_vector

// class my_vector : 
//     boost::additive1< my_vector ,
//     boost::additive2< my_vector , double ,
//     boost::multiplicative2< my_vector , double > > >
// {
//     typedef std::vector< double > vector;

// public:
//     typedef vector::iterator iterator;
//     typedef vector::const_iterator const_iterator;

// public:
//     my_vector( const size_t N )
//         : m_v( N )
//     {
//         m_v.resize( N );
//         init_after_resize(N);
//     }

//     my_vector()
//         : m_v()
//     {
//         int N = 3; 
//         m_v.resize(N);
//         init_after_resize(N);
//     }

// // ... [ implement container interface ]
// //]
//     const double & operator[]( const size_t n ) const {
//         return m_v[n];
//     }

//     double & operator[]( const size_t n ) { 
//         if( (n+1) > this->size() ){
//             resize(n+1);
//         }

//         return m_v[n]; 
//     }

//     iterator begin() { 
//         return m_v.begin();
//     }

//     const_iterator begin() const {
//         return m_v.begin(); 
//     }

//     iterator end() { 
//         return m_v.end(); 
//     }
    
//     const_iterator end() const { 
//         return m_v.end(); 
//     }

//     size_t size() const { 
//         return m_v.size(); 
//     }

//     void resize( const size_t n ) {
//         int last_el = this->size();
//         m_v.resize( n );    
//         init_after_resize(last_el);
//     }

//     my_vector& operator+=( const my_vector &p ) {
//         if(p.size() == 0){
//             return *this;
//         }
        
//         if (p.size() != this->size()) {
//             throw std::invalid_argument("cannot add states of different sizes");
//         }
//         // Add this vector to the input vector
//         // Isn't it elegant ? Thanks stackoverflow. 
//         std::transform(this->begin(), this->end(), p.begin(), this->begin(), std::plus<double>());

//         return *this;
//     }

//     my_vector& operator*=( const double factor )
//     {
//          std::transform(this->begin(), this->end(), this->begin(), 
//             [factor](double el) -> double { return el*factor; });
//         return *this;
//     }

// private:
//     std::vector< double > m_v;

//     // Utility 
//     void init_after_resize(int from){
//         for( size_t i = from; i < this->size(); i++ ){
//             m_v[i] = 0.0;
//         }
//     }
// };

// //[my_vector_resizeable
// // define my_vector as resizeable

// namespace boost { namespace numeric { namespace odeint {

// template<>
// struct is_resizeable< my_vector >
// {
//     typedef boost::true_type type;
//     static const bool value = type::value;
// };

// } } }
// //]


// my_vector operator/( const my_vector &p1 , const my_vector &p2 )
// {
//     my_vector result(p1.size());

//     std::transform(p1.begin(), p1.end(), p2.begin(), result.begin(), 
//             std::divides<double>());

//     return my_vector();
// }

// my_vector abs( const my_vector &p )
// {
//     my_vector result(p.size());
//     float (*fabs)(float) = &std::fabs; 

//     std::transform(p.begin(), p.end(), result.begin(), 
//             fabs);

//     return result;
// }

// // also only for steppers with error control
// namespace boost { namespace numeric { namespace odeint {

// template<>
// struct vector_space_norm_inf< my_vector >
// {
//     static bool abs_compare(float a, float b)
//     {
//         return (std::fabs(a) < std::fabs(b));
//     }
    
//     typedef double result_type;
//     double operator()( const my_vector &v ) const
//     {
//         return *(std::max_element(v.begin(), v.end(), abs_compare));
//     }
// };

// } } }


// typedef my_vector point3D;

// const double sigma = 10.0;
// const double R = 28.0;
// const double b = 8.0 / 3.0;

// void lorenz( const point3D &x , point3D &dxdt , const double t )
// {
//     dxdt[0] = sigma * ( x[1] - x[0] );
//     dxdt[1] = R * x[0] - x[1] - x[0] * x[2];
//     dxdt[2] = -b * x[2] + x[0] * x[1];
// }

// using namespace boost::numeric::odeint;

// int main()
// {
//     point3D x;
//     x[0] = 10.0;
//     x[1] = 5.0; 
//     x[2] = 5.0;

//     // point type defines it's own operators -> use vector_space_algebra !
//     typedef runge_kutta_dopri5< point3D , double , point3D ,
//                                 double , vector_space_algebra > stepper;
//     int steps = integrate_adaptive( make_controlled<stepper>( 1E-10 , 1E-10 ) , lorenz , x ,
//                                     0.0 , 10.0 , 0.1 );
//     // std::cout << x << std::endl;
//     std::cout << "steps: " << steps << std::endl;
// }