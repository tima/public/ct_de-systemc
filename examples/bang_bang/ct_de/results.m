% Load data
trace_1 = load("circuit_1.dat");
trace_2 = load("circuit_2.dat");
trace_ctde = load("circuit.dat");
load("../matlab/simulink_switched.mat");
t_matlab = ans(1,:);
sw_matlab = ans(2,:);
vc_matlab = ans(3,:);

% Common vars
max_time = 1.8;
max_threshold = ones(2)*0.4;
min_threshold = ones(2)*0.2;
t_threshold = [0 max_time];


% Normalize switch status to [0 (OFF), 0.5 (ON)]
sw_matlab = ((-1*sw_matlab)./2+0.5)./2;
sw_bigstep = trace_1(:,3)./2;
sw_smallstep = trace_2(:,3)./2;


figure(1);
hold on;
grid on;    
set(gca,'XTick',0:0.1:max_time);
set(gca,'YTick',0:0.05:0.5);
axis([0 max_time -0.002 0.502]);

plot(trace_2(1:1:end,1), trace_2(1:1:end,2), '-g', 'LineWidth', 2);
plot(trace_ctde(:,1), trace_ctde(:,2), '-ob', 'LineWidth', 2);
plot(t_matlab, vc_matlab, '-k', 'Linewidth', 2 );

stairs(trace_1(:,1), sw_bigstep, '-r', 'LineWidth', 1);
% stairs(trace_2(:,1), sw_smallstep, '--g', 'LineWidth', 1);
% stairs(t_matlab, sw_matlab, '--k', 'LineWidth', 1);

stairs(t_threshold, max_threshold, '--k', 'Linewidth', 2);
stairs(t_threshold, min_threshold, '--k', 'Linewidth', 2);

% yyaxis right;
% yticks([0.00 1]);
% yticklabels({'OFF', 'ON'});
% ylabel("Switch status", 'Color', 'r');


legend("Vc TDF with timestep 5ms", "Vc CT/DE", "Vc MATLAB/Simulink", "Switch timestep 100ms" );

drawnow();

input("Press enter to exit");