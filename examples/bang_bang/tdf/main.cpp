#include<systemc>
#include<systemc-ams>
#include<iostream>

#include<string>

#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */


// Switched RC circuit
SC_MODULE(CircuitRC){
    // Switch control input signal
    sc_core::sc_in<bool> in_switch;
    sc_core::sc_out<double> v_out;


    CircuitRC(sc_core::sc_module_name name, double t_ms = 5) :
        r("r", 0.5), sw("sw", 0.5), c("c", 1), 
        v("v", 0.0, 1.0), n1("n1"), vc("vc"), gnd("gnd"), c_voltage("voltage")
    {
        // Voltage source
        v.n(gnd);
        v.p(n1);

        // Switch + resistor sw.r
        // in series with v
        sw.n(n1);
        sw.p(vc);
        sw.ctrl(in_switch);

        // Resistor r
        r.p(vc);
        r.n(gnd);

        // Capacitor in parallel with resistor r
        c.p(vc);
        c.n(gnd);

        // Converter module from voltage to signal
        c_voltage.p(vc);
        c_voltage.n(gnd);
        c_voltage.outp(v_out);

        // Set a timestep for the module
        // by setting it to one of its 
        // primitive components
        r.set_timestep(t_ms, sc_core::SC_MS);
    };


    void start_of_simulation(){
        // Start tracing a tabular file
        std::string filename = std::string(name()) + std::string(".dat");
        atf = sca_util::sca_create_tabular_trace_file(filename.c_str());
        std::string vc_name("vc");
        sca_trace(atf, vc,vc_name + name());
        sca_trace(atf, in_switch, name());
    }

    void end_of_simulation(){
        // Close tracing file
        sca_util::sca_close_tabular_trace_file(atf);
    }

    private:
        // Resistor
        sca_eln::sca_r r; 
        // Switch
        sca_eln::sca_de::sca_rswitch sw;
        // Capacitor
        sca_eln::sca_c c;
        // Voltage source
        sca_eln::sca_vsource v;
        // Other connection nodes
        sca_eln::sca_node n1, vc;
        // Reference node
        sca_eln::sca_node_ref gnd;
        // Trace file handler
        sca_util::sca_trace_file * atf;
        // Converter from node voltages to output signal
        sca_eln::sca_de_vsink c_voltage;

};

// Module that generates
// the switch control signal
SC_MODULE(SwitchDriverDE){
    sc_core::sc_out<bool> ctrl;
    sc_core::sc_in<double> v_in;

    SC_CTOR(SwitchDriverDE){
        SC_THREAD(ctrl_thread);
        ctrl.initialize(true);
    }

    void ctrl_thread(){
        // The value during the first cycle
        // is set by the initialize method in the constructor
        // So we skip the first cycle with a wait statement
        while(true){
            sc_core::wait(v_in->value_changed_event());
            // sc_core::wait(5, sc_core::SC_MS);
            if (v_in.read() >= 0.40 && ctrl.read()) {
                ctrl.write(false);
            }
            else if ((v_in.read() <= 0.2) && !ctrl.read()) {
                ctrl.write(true);
            }
        }
    }
};


int sc_main(int argc, char* argv[]){

    clock_t t = clock();

    sc_core::sc_set_time_resolution(100.0, sc_core::SC_NS);

    sc_core::sc_signal<bool> ctrl1_sig, ctrl2_sig;
    sc_core::sc_signal<double> v1_sig, v2_sig;

    // Problematic circuit, timestep 200 ms
    CircuitRC circuit_1("circuit_1", 5);
    // Naive solution, timestep 5 ms
    // CircuitRC circuit_2("circuit_2", 5);

    SwitchDriverDE driver_1("driver_1");
    // SwitchDriverDE driver_2("driver_2");


    // Interconnections
    driver_1.ctrl(ctrl1_sig);
    driver_1.v_in(v1_sig);
    circuit_1.in_switch(ctrl1_sig);
    circuit_1.v_out(v1_sig);

    // driver_2.ctrl(ctrl2_sig);
    // driver_2.v_in(v2_sig);
    // circuit_2.in_switch(ctrl2_sig);
    // circuit_2.v_out(v2_sig);

    // Start simulation for a given time
    sc_start(2, sc_core::SC_SEC);


    // Finish simulation
    sc_core::sc_stop();


    t = clock() - t;
    printf ("It took me %d clicks (%f seconds).\n", (int) t,((float)t)/CLOCKS_PER_SEC);

    return 0;
}