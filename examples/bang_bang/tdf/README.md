# SystemC DE - SystemC AMS Synchronization Problems

![Problematic Circuit](doc/images/synch_problems/circuit.png)

Consider the RC circuit shown in the image above.
It is composed of a voltage source _v_, a switch _sw_, two  resistors _sw.r_ and _r_, and a capacitor _c_.
The switch can be either open or closed depending on a control signal received from a driver module modeled in SystemC DE.

Initially, the capacitor has no charge. When the switch is closed at time 0 s, the voltage source is connected to the resistor and capacitor to the right of the diagram. The source causes current to flow through the circuit and charge to be accumulated by the capacitor.

When the switch is opened at time 1.205 s, the voltage source
is disconnected from the circuit to the right. The resistor and capacitor remain interconnected. Voltage across the charged capacitor produces current to continue flowing through the resistor. The capacitor discharges gradually. If the switch is not closed again, the capacitor discharges completely and no more current flows through the resistor.

The described circuit can be modeled in SystemC AMS
by using the primitive voltage source, switch, resistor and capacitor models from the ELN model of computation.

To simulate continuous time models, SystemC AMS requires users to select a time step. This time step is used as:
1. The initial step size for the differential equations solver, which is dynamically reduced until the integration algortihm converges
2. The interval to communicate SystemC AMS models with SystemC DE, which is fixed

The second case poses the following synchronization problem:

If we select a time step of 0.2 s, for example, the circuit will read the switch control value at times 0 s, 0.2 s, 0.4 s, and so on. At time 1.2 s, the value of the switch is read as closed and SystemC AMS is ready to simulate the continuous circuit from 1.2 s to 1.4 s, it's next synchronization point. Nevertheless, as seen in the image below, the switch is opened at time 1.205 s. In a real circuit, this would produce the capacitor to start discharging immediately. In our simulation, the new switch state is read by SystemC at time 1.4 s (0.195 s after it was open), so the simulated capacitor continues charging up until this time as if the switch was still closed. The difference in the dynamics between the real system and the simulation are remarkable. 

The problem arises from the fact that SystemC AMS uses discrete time semantics to synchronize continuous time models (ELN and LSF) with SystemC DE. If a time step is to be used, the continuous time simulation should be informed of events happening in the middle of a time step, for it to be readjusted to consider the implications of such events in the continuous time simulation.

As can be seen in the image below, a naive solution is to reduce the time step of the circuit so that changes in the switch are perceived with a negligible delay. The problem with this solution is that having a smaller than needed time step introduces several useless synchronization points between SystemC AMS and SystemC DE, which may degrade simulation performance. In our case, if we choose a new time step of 0.005 s, there are 1.205/0.005 + 1 = 242 synchronization points in front of just 1.200/0.200 + 1 = 7 synchronization points for a time step of 0.200 s. 

In the image below, we can see the simulation results.
![Results](doc/images/synch_problems/result.png)