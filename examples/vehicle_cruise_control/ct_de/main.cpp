#include <iostream>
#include <fstream>
#include <string>
#include <systemc>
#include <time.h>       /* clock_t, clock, CLOCKS_PER_SEC */

#include "vehicle_ct.h"
#include "transmission_control_unit.h"


#include "sync_conf_utility.h"

// Pass as first parameter y to use the adaptive sync. strategy,
// y as second parameter to avoid rollbacks (use time of the next
// event), and a real number as the third parameter to set the timestep
int sc_main(int argc, char *argv[]){
    clock_t t = clock();

    sc_core::sc_time simulated_time(30, sc_core::SC_SEC);
    bool use_adaptive = true;
    bool avoid_rollback = false;
    double timestep = 0;

    get_sync_conf(argc, argv, use_adaptive, avoid_rollback, timestep);

    VehicleCt vehicle_ct("vehicle_ct", use_adaptive, avoid_rollback, timestep);
    TransmissionControlUnit tcu("tcu");


    sc_core::sc_signal<double> gear_sig("gear_sig"), speed_sig("speed");
    sc_core::sc_signal<double> gear_ratio_sig("gear_ratio_sig");
    sc_core::sc_signal<ShiftingThresholds> threshold_sig("threshold_sig");
    sc_core::sc_event_queue up_evq("up_evq");

    vehicle_ct.update_out(up_evq);
    vehicle_ct.threshold_out(threshold_sig);
    vehicle_ct.gear_in(gear_sig);
    vehicle_ct.gear_ratio_in(gear_ratio_sig);
    vehicle_ct.speed_out(speed_sig);

    tcu.update_in(up_evq);
    tcu.threshold_in(threshold_sig);
    tcu.gear_out(gear_sig);
    tcu.gear_ratio_out(gear_ratio_sig);

    sc_start(simulated_time);
    sc_core::sc_stop();

    t = clock() - t;

    print_sync_stats(use_adaptive, avoid_rollback, timestep, simulated_time.to_seconds(), ((double)t)/CLOCKS_PER_SEC);

    return 0;
}