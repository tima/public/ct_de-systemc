#ifndef CT_MODULE_H
#define CT_MODULE_H


#include <systemc>

#include "ode_system.h"
#include "synchronization_layer.h"

// THESE ARE NOT SOLVER STEPS, BUT INTEGRATION INTERVAL LENGTHS
// Minimum allowed integration interval length
#define MIN_DELTA_T 0.00000001 // seconds.  
// Default synchronization step.
#define DEFAULT_DELTA_T 0.1 // seconds.

// This class abstracts out common operations 
// in all CT Modules : 
// 1. Creation of a synchronization layer object
// 2. Setting up of the initial conditions of the system

namespace sct_core{  
    class ct_module : public sc_core::sc_module, public sct_kernel::ode_system {
        public:
            ct_module(bool use_adaptive_sync_step = true, bool use_time_to_next_event = false);

            // TO BE MADE AUTOMATICALLY
            void end_of_elaboration();
            void end_of_simulation();


            // To be implemented by the user, 
            // as in TDF modules of SystemC AMS
            // e.g. call set_max_timestep
            // The default behavior is to set 
            // a default maximum timestep
            virtual void set_sync_parameters();

            // Set the maximum allowed synchronization timestep
            void set_max_timestep(double max);

            void set_initial_conditions();


            void use_adaptive_sync(bool);
            void avoid_rollbacks(bool);

        private:
            double max_step;
            bool use_adaptive_sync_step;
            bool use_time_to_next_event;
            sct_kernel::synchronization_layer *sync_layer;
            
    };
}

#endif